﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarIMGUI : MonoBehaviour
{
    public float health = 0.0f;
    private float curHealth;

    private Rect healthBar;
    private Rect healthUp;
    private Rect healthDown;

    public Slider healthSlider;

    void Start()
    {
        healthBar = new Rect(Screen.width - 220, 20, 200, 50);
        healthUp = new Rect(Screen.width - 220, 50, 70, 40);
        healthDown = new Rect(Screen.width - 90, 50, 70, 40);
        curHealth = health;
    }

    void OnGUI()
    {
        if (GUI.Button(healthUp, "live up"))
        {
            curHealth = curHealth + 0.1f > 1.0f ? 1.0f : curHealth + 0.1f;
        }
        if (GUI.Button(healthDown, "live down"))
        {
            curHealth = curHealth - 0.1f < 0.0f ? 0.0f : curHealth - 0.1f;
        }

        health = Mathf.Lerp(health, curHealth, 0.05f);

        GUI.HorizontalScrollbar(healthBar, 0.0f, health, 0.0f, 1.0f);

        healthSlider.value = health;
    }
}