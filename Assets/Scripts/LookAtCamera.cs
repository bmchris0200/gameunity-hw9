﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    void Update()
    {
        this.transform.rotation = Quaternion.LookRotation(Vector3.forward);
    }
}
