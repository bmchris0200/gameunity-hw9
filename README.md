# ReadMe

## 文件介绍

本项目是3D游戏编程与设计的第九次作业的项目文件。Assets文件夹是资源文件夹，演示视频.mp4是项目的演示视频。

## 配置说明

1. 打开unity，新建项目，将本项目的Assets文件夹替换掉创建的项目的Assets文件夹。
2. 创建一个默认的Plane对象。
3. 将Prefabs文件夹中的Chris对象拖入场景中。
4. 将Scripts文件夹中的HealthBarIMGUI脚本挂载到Main Camera中。
5. 运行游戏

## 操作说明

​	通过wsad或方向键控制角色，右上角的“life up”和“life down”按钮可以控制生命值升降。



## 设计

设计过程详情见[我的博客](https://blog.csdn.net/weixin_43958631/article/details/111244192)